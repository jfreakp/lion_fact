from enum import Enum


class TypeOrganization(Enum):
    PERSON = 'PERSON'
    OTHER = 'OTHER'

    @classmethod
    def choices(cls):
        return [(i, i.value) for i in cls]
