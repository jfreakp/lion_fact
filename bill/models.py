from django.contrib.auth.models import User
from django.db import models

from common.functions.functions import name_file
from common.types.enumeration import TypeOrganization


class Organization(models.Model):
    image = models.ImageField(upload_to=name_file, blank=True, null=True)
    manager = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=50, null=False)
    ruc = models.CharField(max_length=20, null=False)
    phone = models.CharField(max_length=20, null=False)
    address = models.CharField(max_length=500, null=False)
    type_organization = models.CharField(max_length=20, choices=TypeOrganization.choices(), default=TypeOrganization.PERSON)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Employed(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    address = models.CharField(max_length=500, null=False)
    phone = models.CharField(max_length=20, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Customer(models.Model):
    employed = models.ForeignKey(Employed, on_delete=models.CASCADE)
    identification = models.CharField(max_length=20, null=False)
    name = models.CharField("Name", max_length=60, null=False)
    address = models.CharField(max_length=500, null=False)
    phone = models.CharField(max_length=20, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    user_create = models.IntegerField(blank=False, null=False)
    user_update = models.IntegerField(blank=False, null=False)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    user_create = models.IntegerField(blank=False, null=False)
    user_update = models.IntegerField(blank=False, null=False)
    name = models.CharField(max_length=30, null=False)
    code_bar = models.CharField(max_length=100)
    detail = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    promotion_price = models.DecimalField(max_digits=10, decimal_places=2)
    purchase_price = models.DecimalField(max_digits=10, decimal_places=2)
    promotion = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Bill(models.Model):
    employed = models.ForeignKey(Employed, on_delete=models.CASCADE)
    date_issued = models.DateField(null=False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class BillDetail(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)


