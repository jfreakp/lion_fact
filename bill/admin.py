from django.contrib import admin

from bill.models import Customer, Organization, Employed, Category, Product, Bill

# Register your models here.
admin.site.register(Customer)
admin.site.register(Organization)
admin.site.register(Employed)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Bill)
