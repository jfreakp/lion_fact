from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Customer
from .serializers import CustomerSerializer


@api_view(['GET'])
def customers_list(request):
    if request.method == 'GET':
        data = Customer.objects.all()
        serializer = CustomerSerializer(data, context={'request': request}, many=True)
        return Response(serializer.data)
